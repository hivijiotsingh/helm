Nexastack IaC platform is a container-native workflow engine that provides a common ground to automate the different tasks of Ansible, Helm and Terraform. It provides a simple and human-centric web UI to deploy things on Cloud or On-premise server.

## Nexastack Kubernetes deployment:
### Prerequisite tools:
- Vault
- Nats-server
- Argo
- Database:
   - Redis
   - Postgres
- Mail server
- GitHub oAuth app

### Vault:

Nexastack use vault to store user credentilas like kube config, aws credentilas etc. To deploy Vault you can follow its official document.
Document link: https://www.vaultproject.io/docs/platform/k8s/helm/run.

Once Vault is installed in your k8s cluster, create a kv secret engine with the name "secret" in it.

### Nats-server:
Nats-server is used by Nexastack micorservices for internal communication, the developers provide the config for Nats-server, deploy Nats on kube cluster with the provided config. You can gather knowledge about Nats by following this link: https://docs.nats.io/nats-on-kubernetes/nats-kubernetes.

### Argo:
Nexastack uses argo to run workflows and deploy thing on public as well as private cloud, to deploy argo in your cluster follow its official documentation https://argoproj.github.io/argo-workflows/installation/.

> **_Note:_**  
  Currently it supports argo version 2.7.

### Databases:

Nexastack uses Redis and Postgres database, you can deploy them using bitnami helm charts.

- Postgres:
  https://github.com/bitnami/charts/tree/master/bitnami/postgresql
- Redis:
  https://github.com/bitnami/charts/tree/master/bitnami/postgresql

### Mail Server:

Nexastack uses mail server to delivers email, mail server should be available before deploying the Nexastack microservices.

### GitHub oAuth app:
To integrate Nexastack workflows with GitHub you have to create a GitHub oAuth app, you can visit the following url to create a oAuth app https://github.com/settings/developers.

To understand more about oAuth app you can go through its official documentation https://docs.github.com/en/developers/apps/building-oauth-apps.



### Nexastack Microservices Deployment:

Once the Prerequisite are fullfiled you can begin with Nexastack microservices deployment. You have to configure Nexastack microservices with the above mentioned tools and databases, you can either use Helm chart or manifest files to deploy Nexastack microservices.

#### Nexastack Microservice configurations:
All the configurations will be passed as a config map/secret (secrets are preferable due to sensitive data) in the Nexastack microservices.

Configurations will be passed usin toml format:

  ```
    [Database]
      name = "postgres-username"
      host = "postgres-host"
      port = "postgres-port"
      user = "postgres-user"
      pass = "postgres-password"
      ideal= "ideal-time"
    [GitHub]
      APIAddress = "https://api.github.com/""
      ClientID = "oAUth-clientid"
      ClientKey = "oAuth-clientkey"
      Scopes = "user:email,repo,delete_repo"
      Name= "Boot-iac"
      Email= "iac@boot.com"

    [Mail]
      Host = "mail-host"
      Port = "mail-port"
      From = "senders-address"
      User = "username"
      Pass = "password"
    [Vault]
      Address = "vault-host"
      Token = "vault-root-token"
    [Redis]
      database = "database"
      host = "host"
      port = "port"
      pass = "pass"
    [NatsServer]
      url = "nats-host"
      username = "nats-username"
      password = "nats-password"
  ```
### Nexastack Microservices:
#### Accounts:
Requires:
- Postgres
- Redis
- Vault
- Nats-server
- Mail


  #### Integration engine:
Requires:
- Postgres
- Vault
- Nats-server
- Mail
- GitHub

#### Registry Operations:
Requires:
- Postgres
- Mail
- Redis
- Nats-server

#### Workspace:
Requires:
- Kube-config
- Mail
- Workspace-creation helm chart

#### Workspace UI:
- Front-end microservice.

#### Workspace creation microservices:
Once a user create a workspace from Nexastack UI, it will deploy the three microservice on k8s using helm chart, the helm chart is present in [GitHub](https://github.com/nitinxenonstack/iac-helm.git) and it's url should be provided in workspace microservice.

##### Project:
Requires:
- Postgres
- Mail
- Nats-server

#### Workflow-processing:
Requires:
- Redis
- Nats-server
- Kubeconfig
- Nexastack boilerplate info
